# define base image
FROM openjdk:8-jre-alpine

# docker image maintaner
LABEL maintainer="lindo@develution.co.za"

# set timezone
ENV TZ Africa/Johannesburg

# set jvm command line options
ENV JAVA_OPTS "-Xmx192m -Xms192m -Djava.security.egd=file:///dev/./urandom -XX:+HeapDumpOnOutOfMemoryError"

# make port available outside the container
# EXPOSE 8761

# add application jar
COPY target/*.jar app.jar

HEALTHCHECK --interval=30s --timeout=3s --retries=3 \ 
  CMD curl -f http://localhost/ || exit 1

# specify command to run the jar  
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar /app.jar"]